//
//  Message.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import SwiftUI

struct Message: Identifiable {
    let id = UUID()
    let text: String
}
