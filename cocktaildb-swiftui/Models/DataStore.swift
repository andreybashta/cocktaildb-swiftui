//
//  DataStore.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import Combine
import SwiftUI

final class DataStore: ObservableObject {
    @Published private(set) var data = [Category: [Drink]]()
    @Published private(set) var currentLoadedSections = 0
    @Published private(set) var selectedIndices = [Int]()
    @Published private(set) var error: Message?
    @Published private(set) var isLoading: Bool = false
    
//    private var subscriptions = Set<AnyCancellable>()
    private var categoriesCancellable: AnyCancellable?
    private var drinksCancellable: AnyCancellable?
    
    var currentLastDrinks: [Drink]? {
        drinks.last { $0.count > 0 }
    }
    
    var categories: [Category] {
        Array(data.keys)
    }
    
    var drinks: [[Drink]] {
        data.map { $0.value }
    }
    
    init() {
        fetchCategories()
    }
    
    func setSelectedIndices(with value: [Int]) {
        self.selectedIndices = value
    }
    
    func category(at index: Int) -> Category {
        Array(data.keys)[index]
    }
    
    func drinks(for category: Category) -> [Drink]? {
        data[category]
    }
    
    func fetchCategories() {
        isLoading = true
        let url = URL(string: R.Request.baseURL.rawValue + R.Request.categories.rawValue)!
        categoriesCancellable = URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                    httpResponse.statusCode == 200 else {
                        throw URLError(.badServerResponse)
                    }
                return element.data
                }
            .decode(type: Categories.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                    print ("Cat completion.")
                self.isLoading = false
            },
                  receiveValue: { value in
                    for (index, element) in value.value.enumerated() {
                        self.data[element] = []
                        if index == value.value.endIndex - 1 {
                            self.fetchDrinks(with: self.categories[0])
                        }
                    }
                  }
            )
    }
    
    func fetchDrinks(with category: Category) {
        isLoading = true
        let url = URL(string: (R.Request.baseURL.rawValue + R.Request.drinks.rawValue + category.strCategory).replacingOccurrences(of: " ", with: "_"))!
        drinksCancellable = URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                    httpResponse.statusCode == 200 else {
                        throw URLError(.badServerResponse)
                    }
                return element.data
                }
            .decode(type: Drinks.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                    print ("Drinks completion.")
                    self.isLoading = false
            },
                  receiveValue: {
                    drinks in
                    self.data[category] = drinks.value
                    self.selectedIndices.append(self.currentLoadedSections)
                    self.currentLoadedSections += 1
                  }
            )
    }
    
//    private func calculateIndexPathsToReload(from newDrinks: [Drink]) -> [IndexPath] {
//        let startIndex = 0
//        let endIndex = startIndex + newDrinks.count
//        return (startIndex..<endIndex).map { IndexPath(row: $0, section: self.currentLoadedSections) }
//    }
//
}
