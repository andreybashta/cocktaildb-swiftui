//
//  Category.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import Foundation

struct Categories: Decodable {
    let value: [Category]
    
    enum CodingKeys: String, CodingKey {
        case value = "drinks"
    }
}

struct Category: Identifiable, Decodable, Hashable {
    let id = UUID()
    let strCategory: String
    enum CodingKeys: String, CodingKey {
        case strCategory = "strCategory"
    }
}
