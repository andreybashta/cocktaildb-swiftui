//
//  Drink.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import Foundation

struct Drinks: Decodable {
    let value: [Drink]
    
    enum CodingKeys: String, CodingKey {
        case value = "drinks"
    }
}

struct Drink: Identifiable, Decodable, Hashable {
    let id = UUID()
    let strDrink: String
    let strDrinkThumb: String
    enum CodingKeys: String, CodingKey {
        case strDrink = "strDrink"
        case strDrinkThumb = "strDrinkThumb"
    }
}
