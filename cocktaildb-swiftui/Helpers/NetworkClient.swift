//
//  NetworkClient.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import Foundation
import Combine

typealias Networking = (URLRequest) ->
    AnyPublisher<(data: Data, response: URLResponse), Error>

struct NetworkClient {
    
//    func fetchData<T: Decodable>(with url: URL, completion: @escaping (Result<T, R.DataResponseError>) -> Void) {
//        
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            if let data = data {
//                let decoder = JSONDecoder()
//                if let items = try? decoder.decode(T.self, from: data) {
//                    completion(.success(items))
//                    return
//                }
//            }
//            completion(.failure(.other(description: "Undefined error")))
//        }.resume()
//    }
//    
    let networking: Networking
    
    var categoriesPublisher: AnyPublisher<Categories, Error> {
        let categoriesURL = URL(string: R.Request.baseURL.rawValue + R.Request.categories.rawValue)!
        return networking(.init(url: categoriesURL))
            .map { $0.data }
            .decode(type: Categories.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func drinksPublisher(for category: Category) -> AnyPublisher<Drinks, Error> {
        let drinksURL = URL(string: (R.Request.baseURL.rawValue + R.Request.drinks.rawValue + category.strCategory).replacingOccurrences(of: " ", with: "_"))!
        return networking(.init(url: drinksURL))
            .map { $0.data }
            .decode(type: Drinks.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
}

extension URLSession {
    func erasedDataTaskPublisher(
        for request: URLRequest
    ) -> AnyPublisher<(data: Data, response: URLResponse), Error> {
        dataTaskPublisher(for: request)
            .mapError { $0 }
            .eraseToAnyPublisher()
    }
}
