//
//  Resources.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import Foundation

enum R {
    enum Request: String {
      case baseURL = "https://www.thecocktaildb.com/api/json/v1/1/"
      case categories = "list.php?c=list"
      case drinks = "filter.php?c="
    }
    enum DataResponseError: Error {
      case network
      case decoding
      case other(description: String)
      
      var reason: String {
        switch self {
        case .network:
          return "An error occurred while fetching data "
        case .decoding:
          return "An error occurred while decoding data"
        default:
          return "Error"
        }
      }
    }
}
