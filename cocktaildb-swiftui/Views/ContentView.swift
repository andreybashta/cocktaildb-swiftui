//
//  ContentView.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import SwiftUI
import SDWebImageSwiftUI

struct ContentView: View {
    @StateObject private var dataStore = DataStore()
    @State private var alertMessage: Message?
    
    var body: some View {
        return NavigationView {
            List {
                ForEach(0..<dataStore.currentLoadedSections, id: \.self) { loadedSectionIndex in
                    if dataStore.selectedIndices.contains(loadedSectionIndex) {
                        Section(header: Text(dataStore.categories[loadedSectionIndex].strCategory)) {
                            ForEach(dataStore.data[dataStore.categories[loadedSectionIndex]] ?? [], id: \.id) { drink in
                                HStack {
                                    WebImage(url: URL(string: drink.strDrinkThumb))
                                    .resizable()
                                    .placeholder {
                                        Rectangle().foregroundColor(.gray)
                                    }
                                    .indicator(.activity)
                                    .transition(.fade(duration: 0.5))
                                    .scaledToFit()
                                    .frame(width: 100, height: 100, alignment: .center)
                                    Text(drink.strDrink)
                                        .foregroundColor(.gray)
                                        .onAppear {
                                            if drink == dataStore.currentLastDrinks?.last {
                                                dataStore.fetchDrinks(with: dataStore.category(at: dataStore.currentLoadedSections))
                                            }
                                    }
                                }
                            }
                            
                            if dataStore.isLoading {
                                Spinner(style: .medium)
                            }
                        }
                    } else {
                        Spinner(style: .medium)
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .animation(.linear)
            .onReceive(self.dataStore.$error, perform: { error in
                self.alertMessage = error
            })
            .alert(item: $alertMessage) { message in
                Alert(title: Text(message.text))
            }
        }
    }
}

private var loadingIndicator: some View {
    Spinner(style: .medium)
        .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .center)
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
