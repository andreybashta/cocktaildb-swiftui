//
//  cocktaildb_swiftuiApp.swift
//  cocktaildb-swiftui
//
//  Created by Andrey on 19.09.2020.
//

import SwiftUI

@main
struct cocktaildb_swiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
